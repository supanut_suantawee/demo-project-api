import 'babel-polyfill'
import server from '~/server'
import * as admin from 'firebase-admin'
import serviceAccount from '~/sdk/adminsdk.json'

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://fir-project-d67fb.firebaseio.com"
})

admin.firestore().settings({ timestampsInSnapshots: true })

server.listen(4000, function () {
  console.log('listening on port 4000')
})