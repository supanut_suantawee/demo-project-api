import * as admin from 'firebase-admin'

export default app => {
  app.get("/generate/:id", async function(req, res) {
    let lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    const userId = req.params.id
    let db = admin.firestore()
    let data = await db.collection('users').doc(userId).get()

    let account = data.data().account

    if (!account) {
      return res.send('not found')
    }

    for (let i of account) {
      let posts = []
      for (let j = 0; j < Math.floor(Math.random() * 10) + 1; j++) {
        let start = Math.floor(Math.random() * (lorem.length / 2))
        posts.push({
          title: lorem.slice(start, start + 30),
          content: lorem.slice(start, start + 120)
        })
      }
      i.posts = posts
    }

    res.send(account)
  })
}